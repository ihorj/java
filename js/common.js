var avto = [
'Audi',
'BMW',
'Lamborgini',
'Mercedes',
'Range Rover',
'Rolls-Royce',
'Lexus',
'Toyota',
'Infiniti',
'Honda'
];

for (var i = 0; i < avto.length; i++) {
   console.log(avto[i]);
}

avto.splice(5, 2);
console.log(avto);

function compareNumeric(a, b) {
    if (a > b) return -1;
    if (a < b) return 1;
}

avto.sort(compareNumeric);
console.log(avto);

//delete avto[0];
//console.log(avto);

avto.splice(0, 1);
console.log(avto);

console.log(avto[9]);

